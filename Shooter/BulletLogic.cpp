//BulletLogic.cpp

#include "stdafx.h"

#include "BulletLogic.h"

namespace mercury
{
	namespace world
	{
		BulletLogic::BulletLogic()
		{
			type = "bullet";
		}

		BulletLogic::~BulletLogic()
		{

		}

		void BulletLogic::Initialise(sf::Vector2f pDirection, float pSpeed, float pLifespan)
		{
			m_direction = pDirection;
			m_speed = pSpeed;
			m_lifespan = pLifespan;
		}

		void BulletLogic::Update()
		{
			m_owner->position += m_direction * m_speed * mercury::system::access::DELTATIME;
			m_lifespan -= mercury::system::access::DELTATIME;
			if (m_lifespan < 0.0f)
				m_owner->SetDeathFlag(true);
		}

		void BulletLogic::Cleanup()
		{
			Component::Cleanup();
		}

		void BulletLogic::OnCollision(Collider* pCollider)
		{
			(void)pCollider;
		}

	}
}