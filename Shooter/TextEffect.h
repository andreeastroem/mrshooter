//TextEffect.h

#pragma once

#include "Component.h"

namespace mercury
{
	namespace world
	{
		class TextEffect : public Component
		{
		public:
			TextEffect();
			~TextEffect();

			void Cleanup();
			void Update();

			bool Initialise(sf::Text* pText);
		private:

		public:

		private:
			sf::Text* m_text;

			float m_dY;
			float m_originalY;
			float m_Yspeed;
			float m_time;
			float m_Xspeed;
			sf::Color m_colour;

			float m_fadespeed;

			bool m_goingUp;

			float m_expiredTime;

			bool m_update;
		};
	}
}