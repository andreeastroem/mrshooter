// Shooter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Application.h"

#include <vld.h>

#define UNREFERENCED_PARAMETER(P)          \
    /*lint -save -e527 -e530 */ \
	    { \
        (P) = (P); \
	    } \
    /*lint -restore */

int _tmain(int argc, _TCHAR* argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	Application app;

	if (app.Initialise())
	{
		app.Run();
	}

	app.Cleanup();

	return 0;
}