/************************************************************************/
/* Box collider
/************************************************************************/

#pragma once

#include "Collider.h"

namespace mercury
{
	namespace world
	{
		class BoxCollider : public Collider
		{
		public:
			BoxCollider();
			BoxCollider(EColliderType pType);
			~BoxCollider();

			bool Initialise(b2World& pWorld, sf::Vector2f pPosition, sf::Vector2f pSize, 
				bool pDynamic = false, bool pIsSensor = false, float pDegrees = 0.0f);

			void EnterCollision(Collider* pCollider);

			void LeaveCollision(Collider* pCollider);

		private:

		protected:
			b2PolygonShape m_b2shape;
			
		};
	}
}