//GameObjectFactory.cpp

#include "stdafx.h"

#include "GameObjectFactory.h"

#include "GameObject.h"

//Components
#include "Component.h"
//#include "InputComponent.h"
//#include "ActionComponent.h"
#include "BulletLogic.h"
#include "Camera2D.h"
#include "TextComponent.h"
#include "TextEffect.h"

//render components
#include "CircleComponent.h"
#include "SpriteComponent.h"
#include "RectangleComponent.h"
#include "LineComponent.h"

//collider components
#include "BoxCollider.h"
#include "CircleCollider.h"

//Scripts
#include "PlayerScript.h"
#include "StaticObjectScript.h"


namespace mercury
{
	namespace world
	{
		/*
		static variables
		*/
		mercury::system::ObjectManager::ptr GameObjectFactory::m_objectManager;
		b2World* GameObjectFactory::m_world = nullptr;
		unsigned int GameObjectFactory::numberOfPlayers = 0;
		unsigned int GameObjectFactory::numberOfBullets = 0;

		GameObjectFactory::GameObjectFactory()
		{

		}

		GameObjectFactory::~GameObjectFactory()
		{

		}

		/************************************************************************/
		/* public functions
		/************************************************************************/
		void GameObjectFactory::Initialise(system::ObjectManager::ptr pObjectManager)
		{
			m_objectManager = pObjectManager;
		}

		void GameObjectFactory::SetWorld(b2World& pWorld)
		{
			m_world = &pWorld;
		}


		void GameObjectFactory::CreatePlayer(sf::Vector2f pPosition, sf::Color pColour, EFaction pFaction)
		{
			(void)pColour;
			GameObject* actor = new GameObject("player" + std::to_string(numberOfPlayers), pPosition);
			actor->Initialise(pFaction);

			Component* component;
			//visuals
			component = CreateRenderComponent(actor, CIRCLE);
			static_cast<CircleComponent*>(component)->Initialise(pPosition, 20.0f, pColour);
			actor->AttachComponent(system::EComponentType::RENDER, component);
			
			////input
			//component = CreateComponent(actor, "input");
			//static_cast<InputComponent*>(component)->Initialise((system::input::EControlIndex)numberOfPlayers);
			//actor->AttachComponent(system::EComponentType::INPUT, component);
			////action
			//component = CreateComponent(actor, "action");
			//static_cast<ActionComponent*>(component)->Initialise(7, 0.2f);
			//actor->AttachComponent(system::EComponentType::ACTION, component);
			
			//collider
			component = CreateColliderComponent(actor, Collider::CIRCLE);
			static_cast<CircleCollider*>(component)->Initialise(*m_world, pPosition, 20.0f, true);
			actor->AttachComponent(system::EComponentType::COLLIDER, component);
			//camera
			component = CreateComponent(actor, "camera");
			static_cast<Camera2D*>(component)->Initialise(sf::FloatRect(0, 0, 0.5f, 1), numberOfPlayers);
			actor->AttachComponent(system::EComponentType::CAMERA, component);
			//Animator

			//Script
			component = new PlayerScript();
			component->SetOwner(actor);
			static_cast<PlayerScript*>(component)->Initialise();
			actor->AttachComponent(system::EComponentType::SCRIPT, component);

			m_objectManager->AttachObject(actor);

			numberOfPlayers++;
			system::access::numberOfPlayers = numberOfPlayers;
		}

		void GameObjectFactory::CreateBullet(sf::Vector2f pPosition, sf::Vector2f pEndpos, EFaction pFaction)
		{
			GameObject* actor = new GameObject("bullet" + std::to_string(numberOfBullets));
			actor->Initialise(pFaction);

			std::vector<sf::Vector2f> positions;
			positions.push_back(pPosition);
			positions.push_back(pEndpos);

			Component* component;
			component = CreateRenderComponent(actor, LINE);
			static_cast<LineComponent*>(component)->Initialise(positions);
			actor->AttachComponent(system::EComponentType::RENDER, component);

			m_objectManager->AttachObject(actor);

			CreateBulletFeedback(pPosition, sf::Vector2f(), pFaction);

			numberOfBullets++;
		}

		void GameObjectFactory::CreateWall(sf::Vector2f pPosition, sf::Vector2f pSize, sf::Color pColour, float pDegrees)
		{
			GameObject* actor = new GameObject("object", pPosition);
			actor->Initialise(NEUTRAL);

 			Component* component;
			//visuals
			component = CreateRenderComponent(actor, RECTANGLE);
			static_cast<RectangleComponent*>(component)->Initialise(pPosition, pSize, pColour);
			actor->AttachComponent(system::EComponentType::RENDER, component);
			//collider
			component = CreateColliderComponent(actor, Collider::BOX);
			static_cast<BoxCollider*>(component)->Initialise(*m_world, pPosition, pSize, false, false, pDegrees);
			static_cast<BoxCollider*>(component)->SetCollisionGroup(-EFaction::NEUTRAL);
			actor->AttachComponent(system::EComponentType::COLLIDER, component);
			//script
			component = new StaticObjectScript();
			component->SetOwner(actor);
			static_cast<StaticObjectScript*>(component)->Initialise();
			actor->AttachComponent(system::EComponentType::SCRIPT, component);

			static_cast<RectangleComponent*>(actor->GetComponent<RenderComponent>(system::EComponentType::RENDER))->SetCollider(
				actor->GetComponent<Collider>(system::EComponentType::COLLIDER));

			m_objectManager->AttachObject(actor);
		}

		void GameObjectFactory::CreateBackground(std::string pPathName)
		{
			(void)pPathName;
		}

		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/

		Component* GameObjectFactory::CreateComponent(GameObject* pActor, std::string pComponent)
		{
			Component* component;

			system::EComponentType type = system::access::ConvertStringToComponentEnum(pComponent);

			switch (type)
			{
			case mercury::system::INPUT:
				/*component = new InputComponent();
				component->SetOwner(pActor);*/
				//return component;
				break;
			case mercury::system::ACTION:
				/*component = new ActionComponent();
				component->SetOwner(pActor);*/
				//return component;
				break;
			case mercury::system::BULLETLOGIC:
				component = new BulletLogic();
				component->SetOwner(pActor);
				return component;
				break;
			case mercury::system::CAMERA:
				component = new Camera2D();
				component->SetOwner(pActor);
				return component;
				break;
			case mercury::system::TEXTEFFECT:
				component = new TextEffect();
				component->SetOwner(pActor);
				return component;
				break;
			default:
				printf("Could not add component. Inputted type: %s, \n", pComponent);
				return nullptr;
				break;
			}
		}

		Component* GameObjectFactory::CreateRenderComponent(GameObject* pActor, RenderType pType, int pTextureIndex)
		{
			/************************************************************************/
			/* DO NOT FORGET TO RETURN COMPONENT
			/************************************************************************/
			Component* component;

			switch (pType)
			{
			case POINT:
				printf("Not implemented yet. ID: %i\n", static_cast<int>(pType));
				break;
			case LINE:
				component = new LineComponent();
				component->SetOwner(pActor);
				return component;
				break;
			case TRIANGLE:
				printf("Not implemented yet. ID: %i\n", static_cast<int>(pType));
				break;
			case RECTANGLE:
				component = new RectangleComponent();
				component->SetOwner(pActor);
				return component;
				break;
			case CIRCLE:
				component = new CircleComponent();
				component->SetOwner(pActor);
				return component;
				break;
			case POLYGON:
				printf("Not implemented yet. ID: %i\n", static_cast<int>(pType));
				break;
			case SPRITE:
				component = new SpriteComponent(pTextureIndex);
				component->SetOwner(pActor);
				return component;
				break;
			case TEXT:
				component = new TextComponent();
				component->SetOwner(pActor);
				return component;
				break;
			default:
				printf("No such render type. ID: %i\n", static_cast<int>(pType));
				return nullptr;
				break;
			}

			return nullptr;
		}

		Component* GameObjectFactory::CreateColliderComponent(GameObject* pActor, Collider::EColliderType pType)
		{
			Component* component;

			switch (pType)
			{
			case Collider::CIRCLE:
				component = new CircleCollider(pType);
				component->SetOwner(pActor);
				return component;
				break;
			case Collider::BOX:
				component = new BoxCollider(pType);
				component->SetOwner(pActor);
				return component;
				break;
			}

			return nullptr;
		}

		void GameObjectFactory::CreateBulletFeedback(sf::Vector2f pPosition, sf::Vector2f pDirection, EFaction pFaction)
		{
			(void)pDirection;
			GameObject* bulletfeedback = new GameObject("bulletfeedback" + std::to_string(numberOfBullets), pPosition);

			bulletfeedback->Initialise(pFaction);
			
			Component* component;

			component = CreateRenderComponent(bulletfeedback, TEXT);
			static_cast<TextComponent*>(component)->Initialise(pPosition, "blap");
			bulletfeedback->AttachComponent(system::EComponentType::TEXT, component);

			component = CreateComponent(bulletfeedback, "texteffect");
			static_cast<TextEffect*>(component)->Initialise(bulletfeedback->GetComponent<TextComponent>(system::EComponentType::TEXT)->getText());
			bulletfeedback->AttachComponent(system::EComponentType::TEXTEFFECT, component);

			m_objectManager->AttachObject(bulletfeedback);

			component = nullptr;
			bulletfeedback = nullptr;
		}

		sf::Vector2f GameObjectFactory::RaycastAndGetClosest(sf::Vector2f pStartPosition, sf::Vector2f pDirection, float pLength, GameObject* pRaycaster)
		{
			system::RayCastCallbackClosest* rccbc = new system::RayCastCallbackClosest;

			b2Vec2 start, end;

			start.x = pStartPosition.x;
			start.y = -pStartPosition.y;

			end.x = start.x + (pDirection.x * pLength);
			end.y = start.y + (-pDirection.y * pLength);

			m_world->RayCast(rccbc, start, end);

			sf::Vector2f targetpos = rccbc->GetClosest().intersectPoint;

			if (rccbc->GetClosest().body && pRaycaster && pRaycaster->HasComponentOfType<Collider>(system::EComponentType::COLLIDER))
			{
				static_cast<Collider*>(rccbc->GetClosest().body->GetUserData())->EnterCollision(
					pRaycaster->GetComponent<Collider>(system::EComponentType::COLLIDER));
			}

			delete rccbc;
			rccbc = nullptr;

			if (system::Math::Distance(targetpos) == 0)
				return sf::Vector2f(end.x, -end.y);
			else
				return targetpos;

			
		}

	}
}