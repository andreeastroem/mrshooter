
#include "stdafx.h"

#include "PlayerScript.h"
#include "GameObjectFactory.h"

namespace mercury
{
	namespace world
	{

		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		PlayerScript::PlayerScript()
		{
			type = "script";
		}

		PlayerScript::~PlayerScript()
		{

		}

		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		void PlayerScript::Initialise()
		{
			m_collider = m_owner->GetComponent<CircleCollider>(system::EComponentType::COLLIDER);
			m_firerate = 0.2f;
			m_speed = 20;
			//20 is good for release
#ifdef _DEBUG
			m_speed = 40;
#endif // _DEBUG

		}
		void PlayerScript::Cleanup()
		{
			if (m_collider)
				m_collider = nullptr;
		}
		void PlayerScript::Update()
		{
			ReadInput();
			UpdateTimers();

			m_collider->SetVelocity(sf::Vector2f(m_leftXaxis * m_speed, m_leftYaxis * m_speed));
			
			if (system::Math::Distance(m_rightXaxis, m_rightYaxis) > 0.0f)
				m_owner->direction = sf::Vector2f(m_rightXaxis, m_rightYaxis);

			if (m_fire)
				Shoot();
		}
		void PlayerScript::OnCollision(Collider* pCollider)
		{
			(void)pCollider;
		}
		

		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/

		void PlayerScript::ReadInput()
		{
			m_leftXaxis = system::input::XboxController.GetAxisValue(m_index,
				system::input::EJoystickAxis::XAXIS) / 100;
			m_leftYaxis = system::input::XboxController.GetAxisValue(m_index,
				system::input::EJoystickAxis::YAXIS) / 100;

			m_rightXaxis = system::input::XboxController.GetAxisValue(m_index,
				system::input::EJoystickAxis::UAXIS) / 100;
			m_rightYaxis = system::input::XboxController.GetAxisValue(m_index,
				system::input::EJoystickAxis::RAXIS) / 100;

			m_fire = system::input::XboxController.GetAxisValue(m_index,
				system::input::EJoystickAxis::ZAXIS) > 0.0f;
		}


		void PlayerScript::UpdateTimers()
		{
			system::access::UpdateTimer(m_fireTimer);
		}

		void PlayerScript::Shoot()
		{
			if (m_fireTimer > m_firerate)
			{
				//can shoot

				sf::Vector2f target = m_owner->position + (m_owner->direction * 20.0f);
				sf::Vector2f point = GameObjectFactory::RaycastAndGetClosest(target, m_owner->direction, 300, m_owner);
				GameObjectFactory::CreateBullet(target, point, m_owner->GetFaction());

				m_fireTimer = 0.0f;
			}
		}

		
		/*
		----------------------------------------------
		protected functions
		----------------------------------------------
		*/

	}
}