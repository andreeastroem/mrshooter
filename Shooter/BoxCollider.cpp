#include "stdafx.h"

#include "BoxCollider.h"

namespace mercury
{
	namespace world
	{

		BoxCollider::BoxCollider()
		{
			type = "collider";
		}

		BoxCollider::BoxCollider(EColliderType pType)
		{
			m_type = pType;
		}

		BoxCollider::~BoxCollider()
		{
			Collider::Cleanup();
		}

		void BoxCollider::EnterCollision(Collider* pCollider)
		{
			m_owner->OnCollision(pCollider);
		}

		void BoxCollider::LeaveCollision(Collider* pCollider)
		{
			m_owner->ExitCollision(pCollider);
		}

		bool BoxCollider::Initialise(b2World& pWorld, sf::Vector2f pPosition, sf::Vector2f pSize, bool pDynamic, bool pIsSensor, float pDegrees)
		{
			if (!pDynamic)
			{
				m_bodyDef.position.Set(pPosition.x, -pPosition.y);
				m_bodyDef.angle = system::access::DEGTORAD(pDegrees);

				m_body = pWorld.CreateBody(&m_bodyDef);

				m_b2shape.SetAsBox(pSize.x / 2, pSize.y / 2);

				m_fixtureDef.isSensor = pIsSensor;

				m_fixtureDef.shape = &m_b2shape;
				m_fixtureDef.friction = 0.0f;

				m_body->CreateFixture(&m_fixtureDef);
			}
			else
			{
				m_bodyDef.type = b2_dynamicBody;
				m_bodyDef.position.Set(pPosition.x, -pPosition.y);
				m_bodyDef.angularDamping = 0.1f;
				m_bodyDef.angle = system::access::DEGTORAD(pDegrees);

				m_body = pWorld.CreateBody(&m_bodyDef);

				m_b2shape.SetAsBox(pSize.x / 2, pSize.y / 2);

				m_fixtureDef.isSensor = pIsSensor;
				m_fixtureDef.shape = &m_b2shape;
				m_fixtureDef.density = 1.0f;
				m_fixtureDef.friction = 0.3f;

				m_body->CreateFixture(&m_fixtureDef);
			}

			m_body->SetUserData(this);

			return true;
		}

	}
}