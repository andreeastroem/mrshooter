//Access.h

#pragma once

namespace mercury
{
	namespace system
	{
		enum EComponentType
		{
			RENDER,
			ANIMATOR,
			INPUT,
			ACTION,
			COLLIDER,
			BULLETLOGIC,
			CAMERA,
			TEXT,
			TEXTEFFECT,
			SCRIPT,
			ECOMPONENTTYPESIZE
		};
		class access
		{
		public:
			static EComponentType ConvertStringToComponentEnum(std::string pString);
			static float RADTODEG(float pRadians);
			static float DEGTORAD(float pDegrees);
			static void UpdateTimer(float& pTimer);

			static float DELTATIME;

			static unsigned int numberOfPlayers;
			static unsigned int numberOfConnectedGamepads;
			static bool Gamepads[4];
		};

		class Math
		{
		public:
			static float Distance(sf::Vector2f x, sf::Vector2f y);
			static float Distance(float x, float y);
			static float Distance(sf::Vector2f point);
		};
	}
}