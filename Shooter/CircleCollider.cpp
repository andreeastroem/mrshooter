/************************************************************************/
/* Circle collider
/************************************************************************/

#include "stdafx.h"

#include "CircleCollider.h"

namespace mercury
{
	namespace world
	{
		CircleCollider::CircleCollider()
		{

		}

		CircleCollider::CircleCollider(EColliderType pType)
		{
			m_type = pType;
		}

		CircleCollider::~CircleCollider()
		{
			Collider::Cleanup();

			m_body->SetUserData(NULL);
			m_body->DestroyFixture(m_body->GetFixtureList());
			m_body->GetWorld()->DestroyBody(m_body);

			
		}

		void CircleCollider::EnterCollision(Collider* pCollider)
		{
			m_owner->OnCollision(pCollider);
		}

		void CircleCollider::LeaveCollision(Collider* pCollider)
		{
			m_owner->ExitCollision(pCollider);
		}

		bool CircleCollider::Initialise(b2World& pWorld, sf::Vector2f pPosition, float pRadius, bool pDynamic, bool pIsSensor)
		{
			if (!pDynamic)
			{
				//body definition
				m_bodyDef.position.Set(pPosition.x, pPosition.y);
				
				//body
				m_body = pWorld.CreateBody(&m_bodyDef);

				//shape
				m_b2shape.m_radius = pRadius;

				//fixture definition
				m_fixtureDef.shape = &m_b2shape;
				m_fixtureDef.friction = 0.0f;
				m_fixtureDef.isSensor = pIsSensor;
				
				//Create fixture
				m_body->CreateFixture(&m_fixtureDef);

			}
			else
			{
				//body definition
				m_bodyDef.type = b2_dynamicBody;
				m_bodyDef.position.Set(pPosition.x, -pPosition.y);
				m_bodyDef.angularDamping = 0.1f;

				//body
				m_body = pWorld.CreateBody(&m_bodyDef);

				//shape
				m_b2shape.m_radius = pRadius;

				//fixture definition
				m_fixtureDef.isSensor = pIsSensor;
				m_fixtureDef.shape = &m_b2shape;
				m_fixtureDef.density = 1.0f;
				m_fixtureDef.friction = 0.3f;
				

				//Create fixture
				m_body->CreateFixture(&m_fixtureDef);
			}

			m_body->SetUserData(this);

			return true;
		}

	}
}


