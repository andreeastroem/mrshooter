//Line component

#include "stdafx.h"

#include "LineComponent.h"

namespace mercury
{
	namespace world
	{


		LineComponent::LineComponent()
		{
			m_speed = 200;
			m_delayTimer = 0.0f;
			m_delay = 0.0f;
			m_vertices.setPrimitiveType(sf::LinesStrip);
		}

		LineComponent::~LineComponent()
		{

		}

		sf::Drawable* LineComponent::GetRenderData()
		{
			return this;
		}

		void LineComponent::draw(sf::RenderTarget& target, sf::RenderStates states) const
		{
			states.texture = NULL;

			target.draw(m_vertices);
		}

		void LineComponent::Update()
		{
			if (m_delayTimer < m_delay)
			{
				system::access::UpdateTimer(m_delayTimer);
			}
			else
			{
				float X = m_vertices[1].position.x - m_vertices[0].position.x;
				float Y = m_vertices[1].position.y - m_vertices[0].position.y;

				float hypo = sqrtf(X * X + Y * Y);


				m_vertices[0].position.x += (X / hypo) * (m_speed * system::access::DELTATIME);
				m_vertices[0].position.y += (Y / hypo) * (m_speed * system::access::DELTATIME);

				if (system::Math::Distance(m_vertices[0].position, m_vertices[1].position)
					< m_speed * system::access::DELTATIME)
				{
					sf::VertexArray temp;
					for (unsigned int i = 1; i < m_vertices.getVertexCount(); i++)
					{
						temp.append(m_vertices[i]);
					}
					m_vertices = temp;
					/*m_vertices.erase(m_vertices.begin());*/

					if (m_vertices.getVertexCount() < 2)
						m_owner->SetDeathFlag(true);
				}
			}
		}

		void LineComponent::AddVertex(sf::Vector2f pPosition)
		{
			sf::Vertex vertex = sf::Vertex(pPosition);
			if (m_vertices.getVertexCount() == 0)
				vertex.color = sf::Color(255, 255, 255, 0);
			else
				vertex.color = sf::Color(255, 255, 255, 255);

			m_vertices.append(vertex);
		}

		void LineComponent::Cleanup()
		{
			m_vertices.clear();
		}

		bool LineComponent::Initialise(std::vector<sf::Vector2f> pVertices)
		{
			for (unsigned int i = 0; i < pVertices.size(); i++)
			{
				AddVertex(pVertices[i]);
			}

			return true;
		}

		void LineComponent::SetColour(sf::Color pColour)
		{
			for (unsigned int i = 1; i < m_vertices.getVertexCount(); i++)
			{
				m_vertices[i].color = pColour;
			}
		}

		sf::Color LineComponent::GetColour()
		{
			return m_vertices[1].color;
		}

	}
}