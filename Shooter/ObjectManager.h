//ObjectManager.h

#pragma once

#include <memory>
#include <string>

#include "RenderSystem.h"
#include "GameObject.h"

namespace mercury
{
	namespace system
	{
		class CollisionSystem;

		class ObjectManager
		{
			friend class CollisionSystem;
		public:
			typedef std::shared_ptr<ObjectManager> ptr;
			static ptr Create();
			~ObjectManager();

			void AttachObject(world::GameObject* pActor);

			void Update(RenderSystem::ptr& pRenderSystem);
			void Cleanup();

			world::GameObject* FetchObject(std::string pName);
		private:
			ObjectManager();

		public:

		private:
			//GameObject pool
			std::vector<world::GameObject*> m_actors;
		};
	}
}

/*
Create empty objects to vector
add their visuals to render queue
update objects
*/