//TextEffect.cpp

#include "stdafx.h"

#include "TextEffect.h"

namespace mercury
{
	namespace world
	{
		TextEffect::TextEffect()
		{

		}

		TextEffect::~TextEffect()
		{

		}

		void TextEffect::Cleanup()
		{
			if (m_text)
				m_text = nullptr;

			m_flag = true;
		}

		void TextEffect::Update()
		{
			if (m_update)
			{
				if (m_goingUp)
				{
					float deltatime = system::access::DELTATIME;

					m_text->setPosition(m_text->getPosition().x + (m_Xspeed * deltatime),
						m_text->getPosition().y - (m_Yspeed * deltatime));
					m_text->setScale(m_text->getScale() * (1 + deltatime));

					m_colour.a += static_cast<sf::Uint8>(m_fadespeed * deltatime);
					m_text->setColor(m_colour);

					m_expiredTime += deltatime;

					if (m_expiredTime > (m_time - 0.001))
						m_goingUp = false;

				}
				else
				{
					float deltatime = system::access::DELTATIME;

					m_text->setPosition(m_text->getPosition().x + (m_Xspeed * deltatime),
						m_text->getPosition().y + (m_Yspeed * deltatime));
					m_text->setScale(m_text->getScale() * (1 - deltatime));

					m_colour.a -= static_cast<sf::Uint8>(m_fadespeed * deltatime);
					m_text->setColor(m_colour);

					if (m_colour.a <= 5.0f)
						m_update = false;
				}
			}
			else
			{
				m_owner->SetDeathFlag(true);
			}
		}

		bool TextEffect::Initialise(sf::Text* pText)
		{
			m_time = 0.2f;
			m_originalY = pText->getPosition().y;

			m_dY = 55;

			m_Yspeed = m_dY / m_time;
			m_Xspeed = 155;

			m_goingUp = true;

			m_text = pText;

			m_colour.r = m_text->getColor().r;
			m_colour.g = m_text->getColor().g;
			m_colour.b = m_text->getColor().b;
			m_colour.a = 50;
			
			m_fadespeed = (255 - m_colour.a) / m_time;

			m_text->setColor(m_colour);

			m_expiredTime = 0.0f;

			m_update = true;
			return true;
		}

		

	}
}


