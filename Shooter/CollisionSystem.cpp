//CollisionSystem.cpp

#include "stdafx.h"

#include "Collider.h"

#include "CollisionSystem.h"

#include "GameObject.h"
#include "GameObjectFactory.h"

namespace mercury
{
	namespace system
	{
		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		CollisionSystem::CollisionSystem()
		{
			b2Vec2 gravity = b2Vec2(0.0f, 0.0f);
			m_world = new b2World(gravity);

			m_contactListener = new ContactListener();
			m_world->SetContactListener(m_contactListener);

			m_timestep = 1.0f / 60;
			m_velocityIterations = 6;
			m_positionIterations = 2;

			world::GameObjectFactory::SetWorld(*m_world);
		}
		CollisionSystem::~CollisionSystem()
		{
			Cleanup();
		}

		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		CollisionSystem::ptr CollisionSystem::Create()
		{
			return CollisionSystem::ptr(new CollisionSystem());
		}
		void CollisionSystem::Update()
		{
			m_world->Step(m_timestep, m_velocityIterations, m_positionIterations);
		}
		
		void CollisionSystem::Cleanup()
		{
			if (m_world)
			{
				delete m_world;
				m_world = nullptr;
			}
			if (m_contactListener)
			{
				delete m_contactListener;
				m_contactListener = nullptr;
			}
				
		}

		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/

		/************************************************************************/
		/* Contact Listener
		/************************************************************************/
		
		ContactListener::ContactListener()
		{

		}

		void ContactListener::BeginContact(b2Contact* pContact)
		{
			world::Collider* colA = nullptr;
			world::Collider* colB = nullptr;
			void* userdata = pContact->GetFixtureA()->GetBody()->GetUserData();
			if (userdata)
				colA = static_cast<world::Collider*>(userdata);

			userdata = pContact->GetFixtureB()->GetBody()->GetUserData();
			if (userdata)
				colB = static_cast<world::Collider*>(userdata);
			
			if (colA && colB)
			{
				colA->EnterCollision(colB);
				colB->EnterCollision(colA);
			}
			
		}

		void ContactListener::EndContact(b2Contact* pContact)
		{
			world::Collider* colA = nullptr;
			world::Collider* colB = nullptr;
			void* userdata = pContact->GetFixtureA()->GetBody()->GetUserData();
			if (userdata)
				colA = static_cast<world::Collider*>(userdata);

			userdata = pContact->GetFixtureB()->GetBody()->GetUserData();
			if (userdata)
				colB = static_cast<world::Collider*>(userdata);

			if (colA && colB)
			{
				colA->ExitCollision(colB);
				colB->ExitCollision(colA);
			}
		}

	}
}
