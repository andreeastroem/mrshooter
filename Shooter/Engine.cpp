//Engine.cpp

#include "stdafx.h"

#include "Application.h"
#include "EventListener.h"

#include "Engine.h"

#include "ServiceLocator.h"
#include "GameObjectFactory.h"

using namespace mercury;

/*
-----------------------------------------------
Constructors and destructor
-----------------------------------------------
*/
Engine::Engine()
{

}
Engine::~Engine()
{
	Cleanup();
}


/*
----------------------------------------------
public functions
----------------------------------------------
*/
bool Engine::Initialise(sf::RenderWindow& pWindow)
{
	m_renderSystem = system::RenderSystem::Create(pWindow);
#ifdef _DEBUG
	system::Log::Message("RenderSystem created.", system::Log::SUCCESSFUL);
#endif
	m_eventListener = system::EventListener::Create();
#ifdef _DEBUG
	system::Log::Message("EventListener created.", system::Log::SUCCESSFUL);
#endif
	m_objectManager = system::ObjectManager::Create();
#ifdef _DEBUG
	system::Log::Message("ObjectManager created.", system::Log::SUCCESSFUL);
#endif
	m_resourceLoader = system::ResourceLoader::Create();
#ifdef _DEBUG
	system::Log::Message("ResourceLoader created.", system::Log::SUCCESSFUL);
#endif

	int joystick = 0;
	do 
	{
		mercury::system::access::numberOfConnectedGamepads = joystick;
		joystick++;
	} while (sf::Joystick::isConnected(joystick));

	m_sceneSystem = system::SceneSystem::Create();
#ifdef _DEBUG
	system::Log::Message("SceneSystem created.", system::Log::SUCCESSFUL);
#endif
	m_collisionSystem = system::CollisionSystem::Create();
#ifdef _DEBUG
	system::Log::Message("CollisionSystem created.", system::Log::SUCCESSFUL);
#endif

	world::GameObjectFactory::Initialise(m_objectManager);
#ifdef _DEBUG
	system::Log::Message("GameObjectFactory initialised.", system::Log::SUCCESSFUL);
#endif

	system::ServiceLocator<system::RenderSystem>::SetService(m_renderSystem.get());
	system::ServiceLocator<system::EventListener>::SetService(m_eventListener.get());
	system::ServiceLocator<system::ObjectManager>::SetService(m_objectManager.get());
	system::ServiceLocator<system::SceneSystem>::SetService(m_sceneSystem.get());
	system::ServiceLocator<system::CollisionSystem>::SetService(m_collisionSystem.get());
	system::ServiceLocator<system::ResourceLoader>::SetService(m_resourceLoader.get());

	m_sceneSystem->CreateScene("game");

	return true;
}
void Engine::Update()
{
	UpdateDeltatime();

	m_eventListener->Update();
	m_objectManager->Update(m_renderSystem);
	m_renderSystem->Update();
	m_collisionSystem->Update();
	m_sceneSystem->Update();
}
void Engine::Cleanup()
{
}

/*
----------------------------------------------
private functions
----------------------------------------------
*/

void Engine::UpdateDeltatime()
{
	//Create a temporary time variable
	sf::Time tempTime = m_clock.getElapsedTime();

	//set the delta time
	m_deltatime = tempTime.asSeconds() - m_previousTime.asSeconds();
	system::access::DELTATIME = m_deltatime;

	//Change the previous time variable
	m_previousTime = tempTime;
}

/*
----------------------------------------------
protected functions
----------------------------------------------
*/
void Engine::HandleEvent(sf::Event& pEvent)
{
	m_eventListener->HandleEvent(pEvent);
}