//SceneSystem.h

#pragma once

#include <memory>

namespace mercury
{
	namespace gameplay
	{
		class Scene;
	}

	namespace system
	{
		class SceneSystem
		{
		public:
			typedef std::unique_ptr<SceneSystem> ptr;
			static ptr Create();

			~SceneSystem();
			void Update();
			void Cleanup();

			void CreateScene(std::string pStateName);
			void SwitchScene(std::string pStateName);

		private:
			SceneSystem();

		public:

		private:
			std::map<std::string, gameplay::Scene*> m_scenes;
			gameplay::Scene* m_activeScene;
		};
	}
}