#pragma once

#include "ScriptComponent.h"

#include "EventListener.h"
#include "Collider.h"
#include "CircleCollider.h"

namespace mercury
{
	namespace world
	{
		class PlayerScript : public ScriptComponent
		{
		public:

			PlayerScript();
			~PlayerScript();

			void Initialise();
			void Cleanup();
			void Update();
			void OnCollision(Collider* pCollider);

		private:
			void ReadInput();
			void UpdateTimers();

			/*
			Object related
			*/
			float m_rightXaxis;
			float m_rightYaxis;

			/*
			movement
			*/
			float m_leftXaxis;
			float m_leftYaxis;
			float m_speed;
			CircleCollider* m_collider;

			/*
			shooting
			*/
			float m_shootDelay;
			system::input::EControlIndex m_index;
			float m_firerate;
			float m_fireTimer;
			bool m_fire;

			void Shoot();

			/*
			Sound
			*/
			
		protected:
		};
	}
}