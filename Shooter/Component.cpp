//Component.cpp

#include "stdafx.h"

#include "Collider.h"

#include "Component.h"


namespace mercury
{
	namespace world
	{
		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		Component::Component()
		{

		}
		Component::~Component()
		{

		}

		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		void Component::SetOwner(GameObject* pOwner)
		{
			m_owner = pOwner;
		}

		void Component::Cleanup()
		{
			if (m_owner != nullptr)
			{
				m_owner = nullptr;
			}
		}

		void Component::Update()
		{

		}

		void Component::OnCollision(Collider* pCollider)
		{
			(void)pCollider;
		}

		bool Component::getFlag()
		{
			return m_flag;
		}

		void Component::setFlag(bool state)
		{
			m_flag = state;
		}

		void Component::ExitCollision(Collider* pCollider)
		{
			(void)pCollider;
		}

		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/
	}
}