
#include "stdafx.h"

#include "RectangleComponent.h"

namespace mercury
{
	namespace world
	{

		/************************************************************************/
		/* Constructors and deconstructors
		/************************************************************************/
		RectangleComponent::RectangleComponent()
		{
			m_shape = new sf::RectangleShape;
		}

		RectangleComponent::~RectangleComponent()
		{

		}

		bool RectangleComponent::Initialise(sf::Vector2f pPosition, sf::Vector2f pSize, sf::Color pColour)
		{
			m_shape->setSize(pSize);
			m_shape->setOrigin(pSize.x / 2, pSize.y / 2);
			m_shape->setFillColor(pColour);
			m_shape->setPosition(pPosition);
			

			if (m_collider)
				m_shape->setRotation(m_collider->GetRotationInDegrees());
			

			return true;
		}

		/************************************************************************/
		/* Inherited functions
		/************************************************************************/
		sf::Drawable* RectangleComponent::GetRenderData()
		{
			return m_shape;
		}

		void RectangleComponent::Cleanup()
		{
			if (m_shape)
			{
				delete m_shape;
				m_shape = nullptr;
			}
		}

		void RectangleComponent::Update()
		{
			if (m_collider)
			{
				//printf("(collider)Wall visual rotation: %f\n", m_shape->getRotation());
				m_shape->setPosition(m_collider->GetPosition());
				m_shape->setRotation(m_collider->GetRotationInDegrees());
			}
			else
			{
				//printf("Wall visual rotation: %f\n", m_shape->getRotation());
				m_shape->setPosition(m_owner->position);
				m_shape->setRotation(30.0f);
			}
		}

		void RectangleComponent::SetColour(sf::Color pColour)
		{
			m_shape->setFillColor(pColour);
		}

		sf::Color RectangleComponent::GetColour()
		{
			return m_shape->getFillColor();
		}

		

	}
}