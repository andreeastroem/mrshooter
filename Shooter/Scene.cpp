//Scene.cpp

#include "stdafx.h"
#include "Scene.h"

#include "GameObjectFactory.h"

namespace mercury
{
	namespace gameplay
	{
		Scene::Scene()
		{

		}

		Scene::Scene(std::string pName)
		{
			m_name = pName;
		}

		Scene::~Scene()
		{

		}

		void Scene::Update()
		{
			//Check for win/lose conditions
		}

		bool Scene::Initialise()
		{
			//Create world and objects
			if (m_name == "game")
			{
				for (unsigned int i = 0; i <= mercury::system::access::numberOfConnectedGamepads; i++)
				{
					switch (i)
					{
					case 0:
						mercury::world::GameObjectFactory::CreatePlayer(sf::Vector2f(-100, -100), sf::Color::Red, mercury::world::RED);
						break;
					case 1:
						mercury::world::GameObjectFactory::CreatePlayer(sf::Vector2f(100, -100), sf::Color::Blue, mercury::world::BLUE);
						break;
					case 2:
						mercury::world::GameObjectFactory::CreatePlayer(sf::Vector2f(-100, 100), sf::Color::Green, mercury::world::GREEN);
						break;
					case 3:
						mercury::world::GameObjectFactory::CreatePlayer(sf::Vector2f(100, 100), sf::Color::Yellow, mercury::world::YELLOW);
						break;
					default:
						break;
					}
				}

				//outside walls
				mercury::world::GameObjectFactory::CreateWall(sf::Vector2f(975.0f, 0.0f), sf::Vector2f(2000, 50), sf::Color::Red, 90.0f);
				mercury::world::GameObjectFactory::CreateWall(sf::Vector2f(-975.0f, 0.0f), sf::Vector2f(2000, 50), sf::Color::Red, 90.0f);
				mercury::world::GameObjectFactory::CreateWall(sf::Vector2f(0.0f, 1025.0f), sf::Vector2f(2000, 50), sf::Color::Red, 0.0f);
				mercury::world::GameObjectFactory::CreateWall(sf::Vector2f(0.0f, -1025.0f), sf::Vector2f(2000, 50), sf::Color::Red, 0.0f);

				//objects inside (crates etc)
				mercury::world::GameObjectFactory::CreateWall(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(75, 150), sf::Color(210, 105, 30), 45.0f);

			}
			return true;
		}

		void Scene::Cleanup()
		{
			//Tell object manager to clear the objects
		}

		std::string Scene::GetName()
		{
			return m_name;
		}

	}
}