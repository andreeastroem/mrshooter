//SceneSystem.cpp

#include "stdafx.h"

#include "Scene.h"

#include "SceneSystem.h"


namespace mercury
{
	namespace system
	{
		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		SceneSystem::SceneSystem()
		{

		}
		SceneSystem::~SceneSystem()
		{
			Cleanup();
		}

		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		SceneSystem::ptr SceneSystem::Create()
		{
			return SceneSystem::ptr(new SceneSystem());
		}
		void SceneSystem::Update()
		{
			m_activeScene->Update();
		}
		void SceneSystem::Cleanup()
		{
			auto it = m_scenes.begin();
			while (it != m_scenes.end())
			{
				it->second->Cleanup();
				delete it->second;
				it->second = nullptr;
				it++;
			}

			if (m_activeScene != nullptr)
				m_activeScene = nullptr;
		}

		void SceneSystem::CreateScene(std::string pStateName)
		{
			gameplay::Scene* scene = new gameplay::Scene(pStateName);
			m_scenes.insert(std::pair<std::string, gameplay::Scene*>(pStateName, scene));

			if (m_activeScene == nullptr)
			{
				m_activeScene = scene;
				m_activeScene->Initialise();
			}
		}

		void SceneSystem::SwitchScene(std::string pStateName)
		{
			auto it = m_scenes.find(pStateName);
			if (it != m_scenes.end())
			{
				m_activeScene->Cleanup();		//clear the previous scene
				m_activeScene = it->second;		//Set the new active scene
				m_activeScene->Initialise();
			}
			else
				printf("ERROR SWITCHING SCENES. \n");
		}

		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/
	}
}