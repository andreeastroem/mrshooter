//AudioSystem.cpp

#include "stdafx.h"

#include "AudioSystem.h"

using namespace mercury;
using namespace system;

std::map<std::string, sf::SoundBuffer> AudioSystem::m_sounds;
std::string AudioSystem::m_directory = "../resources/sound/";
sf::Music AudioSystem::m_music;

/*
----------------------------------------------
Constructors and deconstructors
----------------------------------------------
*/
AudioSystem::AudioSystem()
{
}
AudioSystem::~AudioSystem()
{
}

/*
----------------------------------------------
public functions
----------------------------------------------
*/

sf::SoundBuffer* AudioSystem::LoadSound(const std::string& pFilename)
{
	auto it = m_sounds.find(pFilename);

	if (it != m_sounds.end())
	{
		return &(it->second);
	}
	else
	{
		sf::SoundBuffer buffer;
		if (!buffer.loadFromFile(m_directory + pFilename + ".wav"))
		{
			printf("Error loading sound. %s", pFilename);
			return nullptr;
		}
		m_sounds.insert(std::pair<std::string, sf::SoundBuffer>(pFilename, buffer));
		
		it = m_sounds.find(pFilename);
		if (it != m_sounds.end())
		{
			return &(it->second);
		}

		return nullptr;
	}
}
void AudioSystem::PlayMusic(const std::string& pFilename)
{
	if (!m_music.openFromFile("../resources/music/" + pFilename))
	{
		printf("Error streaming music. %s", pFilename);
		return;
	}
	m_music.play();
}
void AudioSystem::StopMusic()
{
	m_music.stop();
}

/*
----------------------------------------------
private functions
----------------------------------------------
*/