/************************************************************************/
/* Circle collider
/************************************************************************/

#pragma once

#include "Collider.h"

namespace mercury
{
	namespace world
	{
		class CircleCollider : public Collider
		{
		public:

			CircleCollider();
			CircleCollider(EColliderType pType);
			~CircleCollider();

			bool Initialise(b2World& pWorld, sf::Vector2f pPosition, float pRadius, 
				bool pDynamic = false, bool pIsSensor = false);

			void EnterCollision(Collider* pCollider);
			void LeaveCollision(Collider* pCollider);

			
		private:

		protected:
			b2CircleShape m_b2shape;
		};
	}
}