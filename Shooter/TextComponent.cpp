//Renders 2D text components to the screen

#include "stdafx.h"
#include "TextComponent.h"

namespace mercury
{
	namespace world
	{
		TextComponent::TextComponent()
		{
			m_font = nullptr;
			m_text = nullptr;
		}
		TextComponent::~TextComponent()
		{

		}

		void TextComponent::Cleanup()
		{
			if (m_text != nullptr)
			{
				delete m_text;
				m_text = nullptr;
			}
			if (m_font != nullptr)
			{
				delete m_font;
				m_font = nullptr;
			}

			m_flag = true;
		}

		void TextComponent::Update()
		{
			
		}
		bool TextComponent::Initialise(sf::Vector2f pPosition, std::string pText)
		{
			m_text = new sf::Text;
			m_font = new sf::Font;
			if (!m_text)
			{
				Cleanup();
				return false;
			}

			if (!m_font->loadFromFile("../resources/font/CHERL___.TTF"))
			{
				//error loading font
				printf("Problems occured while trying to load font from: ../resources/font/CHERL___.TTF");
			}
			
			m_text->setColor(sf::Color::Black);
			m_text->setString(pText);
			m_text->setCharacterSize(15);
			m_text->setFont(*m_font);
			m_text->setPosition(pPosition);

			m_flag = false;

			return true;
		}

		void TextComponent::SetTextPosition(const sf::Vector2f& pPosition)
		{
			m_text->setPosition(pPosition);
		}

		void TextComponent::SetTextScale(const sf::Vector2f& pScale)
		{
			m_text->setScale(pScale);
		}

		sf::Text* TextComponent::getText()
		{
			return m_text;
		}

		sf::Drawable* TextComponent::getTextDrawable()
		{
			sf::Drawable* drawable;
			drawable = m_text;
			return drawable;
		}

		sf::Drawable* TextComponent::GetRenderData()
		{
			return m_text;
		}

		void TextComponent::SetString(const char* pText)
		{
			m_text->setString(pText);
		}

		void TextComponent::SetColour(sf::Color pColour)
		{
			m_text->setColor(pColour);
		}

		sf::Color TextComponent::GetColour()
		{
			return m_text->getColor();
		}


	}
}