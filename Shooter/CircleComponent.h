/************************************************************************/
/* A class for rendering circles
/* Functionality can be added.
/************************************************************************/

#pragma once

#include "RenderComponent.h"

namespace mercury
{
	namespace world
	{
		class CircleComponent : public RenderComponent
		{
		public:
			CircleComponent();
			~CircleComponent();

			bool Initialise(sf::Vector2f pPosition, float pRadius, sf::Color pColour);

			//Inherited functions
			sf::Drawable* GetRenderData();
			void Cleanup();
			void Update();

			void SetColour(sf::Color pColour);
			sf::Color GetColour();

		private:

		public:

		private:
			sf::CircleShape* m_circleShape;
		};
	}
}