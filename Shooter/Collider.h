//Collider.h

#pragma once

#include "Component.h"
#include <Box2D.h>

namespace mercury
{
	struct RayCastReport
	{
		b2Body* body;
		float32 fraction;
		sf::Vector2f intersectPoint;
		sf::Vector2f normal;
	};
	namespace system
	{
		class RayCastCallbackClosest : public b2RayCastCallback
		{
		public:
			RayCastCallbackClosest();

			RayCastReport GetClosest();
		private:
			RayCastReport m_report;
		protected:
			float32 ReportFixture(b2Fixture* pFixture, const b2Vec2& pPoint, const b2Vec2& pNormal, float32 pFraction);
		};
	}


	namespace world
	{
		

		class Collider : public Component
		{
		public:
			enum EColliderType
			{
				CIRCLE,
				BOX,
				DANGEROUS,
				HEGRENADE,
				FLASHBANG,
				SMOKEGRENADE,
				ENDOFDANGEROUS,
				PLAYERSPECIFIC,
				ECOLLIDERTYPESIZE
			};

			Collider();
			Collider(EColliderType pType);
			~Collider();

			//Essential functions
			void Initialise();
			void Update();
			void Cleanup();

			virtual void EnterCollision(Collider* pCollider) = 0
			{ 
				(void)pCollider;
			};
			virtual void LeaveCollision(Collider* pCollider) = 0
			{
				(void)pCollider;
			};


			virtual void ApplyForce(sf::Vector2f pForce);
			virtual void SetVelocity(sf::Vector2f pVelocity);
			virtual sf::Vector2f GetVelocity();
			virtual sf::Vector2f GetPosition();
			virtual EColliderType GetType();
			virtual float GetRotationInDegrees();
			virtual void SetRotation(float pDegrees);
			virtual b2Body* GetBody();
			/************************************************************************/
			/* Check the reports body, it will be null if the raycast did not 
			/* intersect any other fixtures
			/************************************************************************/
			virtual RayCastReport RayCast(sf::Vector2f pEndPosition);

			void SetCollisionGroup(int pGroupIndex);

		private:

		protected:

		protected:
			b2Body* m_body;
			b2BodyDef m_bodyDef;
			b2FixtureDef m_fixtureDef;

			EColliderType m_type;
		};
	}
}