//Script for static objects, providing feedback when hit etc


#pragma once

#include "ScriptComponent.h"

namespace mercury
{
	namespace world
	{
		class StaticObjectScript : public ScriptComponent
		{
		public:

			StaticObjectScript();
			~StaticObjectScript();

			bool Initialise();

			void Update();
			void OnCollision(Collider* pCollider);
			void ExitCollision(Collider* pCollider);

		private:

		protected:
			float m_timer;
			float m_time;


			sf::Color m_originalColour;
		};
	}
}