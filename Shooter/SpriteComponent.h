/************************************************************************/
/* A sprite component for rendering a part of a texture.
/* 
/************************************************************************/

#pragma once

#include "RenderComponent.h"

namespace mercury
{
	namespace world
	{
		class SpriteComponent : public RenderComponent
		{
		public:
			//Constructors and deconstructors
			SpriteComponent();
			SpriteComponent(int pTextureIndex);
			~SpriteComponent();

			//Inherited functions
			sf::Drawable* GetRenderData();
			void Update();
			void Cleanup();

			void SetColour(sf::Color pColour);

			sf::Color GetColour();


		private:

		public:

		private:
			sf::Sprite* m_sprite;
		};
	}
}