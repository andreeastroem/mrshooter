
#include "stdafx.h"

#include "StaticObjectScript.h"
#include "RectangleComponent.h"

namespace mercury
{
	namespace world
	{
		StaticObjectScript::StaticObjectScript()
		{
			m_timer = 0.5f;
			m_time = 0.5f;
		}

		StaticObjectScript::~StaticObjectScript()
		{

		}


		bool StaticObjectScript::Initialise()
		{
			m_originalColour = m_owner->GetComponent<RectangleComponent>(system::EComponentType::RENDER)->GetColour();
			return true;
		}


		void StaticObjectScript::Update()
		{
			if (m_timer < m_time)
			{
				system::access::UpdateTimer(m_timer);
				if (m_timer >= m_time)
					m_owner->GetComponent<RectangleComponent>(system::EComponentType::RENDER)->SetColour(m_originalColour);
			}

		}

		void StaticObjectScript::OnCollision(Collider* pCollider)
		{
			(void)pCollider;
			m_timer = 0.0f;
			m_owner->GetComponent<RectangleComponent>(system::EComponentType::RENDER)->SetColour(sf::Color::Green);
		}

		void StaticObjectScript::ExitCollision(Collider* pCollider)
		{
			(void)pCollider;
			throw std::logic_error("The method or operation is not implemented.");
		}


	}
}

