/************************************************************************/
/* Class for rendering rectangles
/************************************************************************/

#pragma once

#include "RenderComponent.h"

namespace mercury
{
	namespace world
	{
		class RectangleComponent : public RenderComponent
		{
		public:

			RectangleComponent();
			~RectangleComponent();

			bool Initialise(sf::Vector2f pPosition, sf::Vector2f pSize, sf::Color pColour);

			//Inherited functions
			sf::Drawable* GetRenderData();
			void Cleanup();
			void Update();

			void SetColour(sf::Color pColour);

			sf::Color GetColour();

		private:
		protected:
			sf::RectangleShape* m_shape;
		};
	}
}