//Engine.h

#pragma once

#include "RenderSystem.h"
#include "EventListener.h"
#include "ObjectManager.h"
#include "AudioSystem.h"
#include "SceneSystem.h"
#include "CollisionSystem.h"
#include "ResourceLoader.h"

class Application;

namespace mercury
{
	namespace system
	{
		class EventListener;
	}
	class Engine
	{
		friend class Application;
	public:
		Engine();
		~Engine();

		bool Initialise(sf::RenderWindow& pWindow);
		void Update();
		void Cleanup();
		
	private:
		void UpdateDeltatime();

	public:

	private:
		system::RenderSystem::ptr m_renderSystem;
		system::EventListener::ptr m_eventListener;
		system::ObjectManager::ptr m_objectManager;
		system::SceneSystem::ptr m_sceneSystem;
		system::CollisionSystem::ptr m_collisionSystem;
		system::ResourceLoader::ptr m_resourceLoader;

		sf::Clock m_clock;
		sf::Time m_previousTime;
		float m_deltatime = 0.0f;

	protected:
		void HandleEvent(sf::Event& pEvent);
	};
}

