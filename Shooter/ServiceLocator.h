//A service locator that is used for accessing different parts of the project
//without the need of complicated hierarchies

#pragma once

namespace mercury
{
	namespace system
	{
		//Only use this for services like Audio system, collision system and so forth
		template <class Service>
		class ServiceLocator
		{
		private:
			ServiceLocator(const ServiceLocator<Service>&);
			ServiceLocator& operator=(const ServiceLocator<Service>&);

		public:
			ServiceLocator(){}

			static void SetService(Service* pService)
			{
				m_service = pService;
			}
			static Service* GetService()
			{
				return m_service;
			}

		private:
			static Service* m_service;
		};

		template <class Service>
		Service* ServiceLocator<Service>::m_service = nullptr;
	}
}