//ObjectManager.cpp

#include "stdafx.h"

#include "CollisionSystem.h"

#include "ObjectManager.h"

#include "RenderComponent.h"
#include "TextComponent.h"

#include "Camera2D.h"

namespace mercury
{
	namespace system
	{
		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		ObjectManager::ObjectManager()
		{
		}
		ObjectManager::~ObjectManager()
		{
			Cleanup();
		}

		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		ObjectManager::ptr ObjectManager::Create()
		{
			return ObjectManager::ptr(new ObjectManager());
		}
		void ObjectManager::Update(RenderSystem::ptr& pRenderSystem)
		{
			if (m_actors.size() == 0)
			{

			}
			for (int i = (m_actors.size() - 1); i >= 0; i--)
			{
				m_actors[i]->Update();

				if (m_actors[i]->GetDeathFlag())
				{
					m_actors[i]->Cleanup();
					delete m_actors[i];
					m_actors[i] = nullptr;
					m_actors.erase(m_actors.begin() + i);
				}
				else
				{
					world::Camera2D* camera = (m_actors[i]->GetComponent<world::Camera2D>(system::EComponentType::CAMERA));
					if (camera)
					{
						int a = (int)camera->id % 2;
						if (camera->id == 0)
						{
							camera->ChangeViewSize(sf::FloatRect(0, 0, 1, 1));
						}
						else if (camera->id < 2)
						{
							if (system::access::numberOfPlayers < 4)
								camera->ChangeViewSize(sf::FloatRect((float)(camera->id / 2), 0, 0.5, 1));
							else
							camera->ChangeViewSize(sf::FloatRect((float)(camera->id / 2), 0, 0.5, 0.5));
						}
						else if (camera->id < 4)
						{
							camera->ChangeViewSize(sf::FloatRect((float)a / 2, 0.5, 0.5, 0.5));
						}
						//camera->ChangeViewSize(sf::FloatRect((float)(a / 2), (int)camera->id / 2, 0.5, 0.5));
						pRenderSystem->AddToCameras(camera->GetView());
					}
					//if object has visuals
					//add those to the queue
					mercury::world::RenderComponent* render = m_actors[i]->GetComponent<mercury::world::RenderComponent>(system::EComponentType::RENDER);
					if (render)
					{
						sf::Drawable* visual = render->GetRenderData();
						pRenderSystem->AddToQueue(visual);
					}
					/*Mercury::world::TextComponent* text = m_actors[i]->GetComponent<Mercury::world::TextComponent>(system::EComponentType::TEXT);
					if (text)
					{
					sf::Drawable* visual = text->getTextDrawable();
					pRenderSystem->AddToQueue(visual);
					}*/
				}
			}
			
			
		}
		mercury::world::GameObject* ObjectManager::FetchObject(std::string pName)
		{
			for (unsigned int i = 0; i < m_actors.size(); i++)
			{
				if (m_actors[i]->name == pName)
					return m_actors[i];
			}

			return nullptr;
		}
		void ObjectManager::Cleanup()
		{
			for (unsigned int i = 0; i < m_actors.size(); i++)
			{
				m_actors[i]->Cleanup();
				delete m_actors[i];
				m_actors[i] = nullptr;
			}
		}

		void ObjectManager::AttachObject(world::GameObject* pActor)
		{
			m_actors.push_back(pActor);
		}

		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/
	}
}
