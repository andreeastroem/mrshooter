/************************************************************************/
/* ResourceLoader.cpp
/* Read ResourceLoader.h head
/************************************************************************/

#include "stdafx.h"

#include "ResourceLoader.h"
#include <fstream>

namespace mercury
{
	namespace system
	{
		/************************************************************************/
		/* Initialisation
		/************************************************************************/

		ResourceLoader::ptr ResourceLoader::Create()
		{
			return ResourceLoader::ptr(new ResourceLoader);
		}

		/************************************************************************/
		/* Resource loading
		/************************************************************************/

		void ResourceLoader::CreateTexture(std::string pPathName)
		{
			sf::Texture tex;
			tex.loadFromFile(pPathName);

			m_mutex.lock();

			m_textureCache.push_back(tex);
		}

		int ResourceLoader::CreateTextureFromFile(const std::string& pPathName)
		{
			//When creating a thread that references member function, give a reference to that class (e.g. this)
			m_textureThread = std::thread(&ResourceLoader::CreateTexture, this, pPathName);

			int id = m_textureCache.size() - 1;

			m_textureThread.join();

			m_mutex.unlock();

			return id;
		}

		int ResourceLoader::CreateSoundFromFile(const std::string& pPathName)
		{
			return 0;
		}

		void ResourceLoader::CreateSound(std::string pPathName)
		{
			(void)pPathName;
		}

		sf::Texture* ResourceLoader::GetTexture(unsigned int pID)
		{
			if (pID < m_textureCache.size())
				return &m_textureCache[pID];
			else
				return nullptr;
		}

		sf::SoundBuffer* ResourceLoader::GetSound(unsigned int pID)
		{
			if (pID < m_soundCache.size())
				return &m_soundCache[pID];
			else
				return nullptr;
		}

		LOAD_RESULT ResourceLoader::LoadSection(const std::string& pFileName, const std::string& pSectionName)
		{
			std::ifstream stream;
			std::string message;

			stream.open("..\resources\config/" + pFileName + ".txt");

			if (!stream.is_open())
			{
				message = "File does not exists. Filename: ..\resources\config/" + pFileName + ".txt";
				Log::Message(message.c_str(), Log::ERROR);

				return FAILURE;
			}

			std::vector<std::string> FileLines;

			unsigned int i = 0;

			while (!stream.eof())
			{
				std::string Line;
				std::getline(stream, Line, '\n');

				//no comments
				if (Line.find('#') != Line.npos)
				{
					Line = Line.substr(0, Line.find('#'));
				}

				//no empty lines
				if (Line.length() == 0)
					continue;

				//Section
				if (FindSection(pSectionName, Line))
				{

				}

				FileLines.push_back(Line);
				i++;
			}

			return SUCCESS;
		}


		bool ResourceLoader::FindSection(const std::string& pSectionName, std::string pLine)
		{
			std::size_t start = pLine.find('[');
			std::size_t close = pLine.find(']');
			if (start != pLine.npos && close != pLine.npos)
			{
				pLine = pLine.substr(start, close - start);
				if (pLine == pSectionName)
					return true;
			}
			return false;
		}


		LOAD_RESULT ResourceLoader::LoadInt(const std::string& pSection, const std::string& pVariableName, int& pValue)
		{
			return SUCCESS;
		}

		LOAD_RESULT ResourceLoader::LoadBool(const std::string& pSection, const std::string& pVariableName, bool& pValue)
		{
			return SUCCESS;
		}

	}
}