//CollisionSystem.h

#pragma once


#include <memory>
#include <string>

#include "ObjectManager.h"

#include <Box2D.h>

namespace mercury
{
	namespace world
	{
		class Collider;
	}

	namespace system
	{
		class ContactListener;

		class CollisionSystem
		{
		public:
			typedef std::unique_ptr<CollisionSystem> ptr;
			static ptr Create();
			~CollisionSystem();

			void Update();
			void Cleanup();

		private:
			CollisionSystem();

		public:

		private:
			b2World* m_world;
			ContactListener* m_contactListener;

			float32 m_timestep;
			int32 m_velocityIterations;
			int32 m_positionIterations;
		};

		/************************************************************************/
		/* Contact Listener
		/************************************************************************/
		class ContactListener : public b2ContactListener
		{
		public:
			ContactListener();

		private:

		protected:

			void BeginContact(b2Contact* pContact);

			void EndContact(b2Contact* pContact);

		};

		/************************************************************************/
		/* Collision filtrering
		/************************************************************************/
		
	}
}

