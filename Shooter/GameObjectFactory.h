//GameObjectFactory.h

#pragma once

#include "ObjectManager.h"

#include "RenderComponent.h"
#include "Collider.h"

#include <Box2D.h>

#include <memory>

namespace mercury
{
	namespace world
	{
		class GameObjectFactory
		{
		public:
			GameObjectFactory();
			~GameObjectFactory();

			static void Initialise(system::ObjectManager::ptr pObjectManager);
			static void SetWorld(b2World& pWorld);

			static void CreatePlayer(sf::Vector2f pPosition, sf::Color pColour, EFaction pFaction);
			static void CreateBullet(sf::Vector2f pPosition, sf::Vector2f pEndpos, EFaction pFaction);
			static void CreateWall(sf::Vector2f pPosition, sf::Vector2f pSize, sf::Color pColour, float pDegrees = 0.0f);
			static void CreateBackground(std::string pPathName);

			static sf::Vector2f RaycastAndGetClosest(sf::Vector2f pStartPosition, sf::Vector2f pDirection, float pLength, GameObject* pRaycaster = nullptr);

		private:
			static system::ObjectManager::ptr m_objectManager;
			static b2World* m_world;
			static unsigned int numberOfPlayers;
			static unsigned int numberOfBullets;

			static Component* CreateComponent(GameObject* pActor, std::string pComponent);
			static Component* CreateRenderComponent(GameObject* pActor, RenderType pType, int pTextureIndex = 0);
			static Component* CreateColliderComponent(GameObject* pActor, Collider::EColliderType pType);

			static void CreateBulletFeedback(sf::Vector2f pPosition, sf::Vector2f pDirection, EFaction pFaction);
		};
	}
}