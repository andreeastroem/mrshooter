//SpriteComponent.cpp

#include "stdafx.h"

#include "SpriteComponent.h"

namespace mercury
{
	namespace world
	{


		SpriteComponent::SpriteComponent()
		{

		}

		SpriteComponent::SpriteComponent(int pTextureIndex)
		{
			(void)pTextureIndex;
		}

		SpriteComponent::~SpriteComponent()
		{

		}

		sf::Drawable* SpriteComponent::GetRenderData()
		{
			return m_sprite;
		}

		void SpriteComponent::Cleanup()
		{
			if (m_sprite != nullptr)
			{
				delete m_sprite;
				m_sprite = nullptr;
			}
		}

		void SpriteComponent::Update()
		{
			m_sprite->setPosition(m_owner->position);
		}

		void SpriteComponent::SetColour(sf::Color pColour)
		{
			m_sprite->setColor(pColour);
		}

		sf::Color SpriteComponent::GetColour()
		{
			return m_sprite->getColor();
		}

	}
}