//Scene.h

#pragma once

namespace mercury
{
	namespace gameplay
	{
		class Scene
		{
		public:
			Scene();
			Scene(std::string pName);
			~Scene();

			bool Initialise();		//Loads all components when booted
			void Update();			//Checks for win/lose condition
			void Cleanup();			//Saves important information (who won etc) then discards objects
			
			std::string GetName();
		private:

		public:

		private:
			std::string m_name;
		};
	}
}