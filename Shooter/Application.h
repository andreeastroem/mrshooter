//Application.h

#pragma once

namespace mercury
{
	class Engine;
};

class Application
{
public:
	Application();
	~Application();

	bool Initialise();
	void Run();
	void Cleanup();

private:
	bool ReadConfig();
	void HandleEvent(sf::Event& pEvent);
public:
	
private:
	int m_windowHeight, m_windowWidth, m_windowAA;
	sf::RenderWindow m_window;
	sf::ContextSettings m_windowSettings;

	mercury::Engine* m_engine;
};