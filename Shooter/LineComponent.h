//Rendering a line between two or more points

#pragma once

#include "RenderComponent.h"

namespace mercury
{
	namespace world
	{
		class LineComponent : public RenderComponent, public sf::Drawable
		{
		public:
			LineComponent();
			~LineComponent();

			sf::Drawable* GetRenderData();

			bool Initialise(std::vector<sf::Vector2f> pVertices);

			void SetDelay(float pDelay);
			void AddVertex(sf::Vector2f pPosition);
			void draw(sf::RenderTarget& target, sf::RenderStates states) const;

			void Update();

			void Cleanup();

			void SetColour(sf::Color pColour);

			sf::Color GetColour();

		private:

		public:

		private:
			sf::VertexArray m_vertices;
			float m_speed;
			float m_delay;
			float m_delayTimer;
		};
	}
}