//BulletLogic.cpp

#include "stdafx.h"

#include "Collider.h"

#define PI 3.14159265359

namespace mercury
{
	namespace system
	{
		RayCastCallbackClosest::RayCastCallbackClosest()
		{
			m_report.fraction = 20150124;
			m_report.body = nullptr;
		}

		RayCastReport RayCastCallbackClosest::GetClosest()
		{
			return m_report;
		}

		float32 RayCastCallbackClosest::ReportFixture(b2Fixture* pFixture, const b2Vec2& pPoint, const b2Vec2& pNormal, float32 pFraction)
		{
			if (m_report.body)
			{
				if (pFraction < m_report.fraction)
				{
					m_report.body = pFixture->GetBody();
					m_report.fraction = pFraction;
					m_report.intersectPoint = sf::Vector2f(pPoint.x, -pPoint.y);
					m_report.normal = sf::Vector2f(pNormal.x, -pNormal.y);

				}
			}
			else
			{
				m_report.body = pFixture->GetBody();
				m_report.fraction = pFraction;
				m_report.intersectPoint = sf::Vector2f(pPoint.x, -pPoint.y);
				m_report.normal = sf::Vector2f(pNormal.x, -pNormal.y);
			}

			return 1;
		}

	}

	namespace world
	{
		Collider::Collider()
		{
			type = "collider";
		}

		Collider::Collider(EColliderType pType)
		{
			m_type = pType;
		}

		Collider::~Collider()
		{

		}

		void Collider::Initialise()
		{
		}

		void Collider::Update()
		{
			m_owner->position = sf::Vector2f(m_body->GetPosition().x, -m_body->GetPosition().y);
		}

		void Collider::Cleanup()
		{
			Component::Cleanup();

			if (m_body)
				m_body = nullptr;
		}

		void Collider::ApplyForce(sf::Vector2f pForce)
		{
			m_body->ApplyForce(b2Vec2(pForce.x, pForce.y), m_body->GetPosition(), true);
		}

		void Collider::SetVelocity(sf::Vector2f pVelocity)
		{
			m_body->SetLinearVelocity(b2Vec2(pVelocity.x, -pVelocity.y));
		}

		sf::Vector2f Collider::GetVelocity()
		{
			return sf::Vector2f(m_body->GetLinearVelocity().x, m_body->GetLinearVelocity().y);
		}

		sf::Vector2f Collider::GetPosition()
		{
			return sf::Vector2f(m_body->GetPosition().x, -m_body->GetPosition().y);
		}

		RayCastReport Collider::RayCast(sf::Vector2f pEndPosition)
		{
			system::RayCastCallbackClosest rccc;
			m_body->GetWorld()->RayCast(&rccc, m_body->GetPosition(), b2Vec2(pEndPosition.x, -pEndPosition.y));
			RayCastReport rcr = rccc.GetClosest();
			
			return rcr;
		}

		Collider::EColliderType Collider::GetType()
		{
			return m_type;
		}

		void Collider::SetCollisionGroup(int pGroupIndex)
		{
			b2Filter filter = m_body->GetFixtureList()->GetFilterData();
			filter.groupIndex = static_cast<sf::Int16>(pGroupIndex);
			m_body->GetFixtureList()->SetFilterData(filter);
		}

		float Collider::GetRotationInDegrees()
		{
			return system::access::RADTODEG(m_body->GetAngle());
		}

		void Collider::SetRotation(float pDegrees)
		{
			m_body->SetTransform(m_body->GetPosition(), system::access::DEGTORAD(pDegrees));
		}

		b2Body* Collider::GetBody()
		{
			return m_body;
		}

	}
}