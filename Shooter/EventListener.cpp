//InputSystem.cpp

#include "stdafx.h"

#include "EventListener.h"

#include "Engine.h"


#include <iostream>

//namespaces
using namespace mercury::system;

//Global variables

/*
----------------------------------------------
Constructors and deconstructors
----------------------------------------------
*/
EventListener::EventListener()
{
	/**/
	input::GamepadButton button;
	for (int j = 0; j < input::EControlIndex::CONTROLINDEX; j++)
	{
		button.controlIndex = (input::EControlIndex)j;
		for (int i = 0; i < input::EButton::EBUTTONSIZE; i++)
		{
			button.button = (input::EButton)i;
			button.state = false;
			input::XboxController.SetCurrentButtonPress(button.controlIndex, button.button, button.state);
			input::XboxController.SetPreviousButtonPress(button.controlIndex, button.button, button.state);
		}
	}

	input::GamepadAxis axis;
	for (int j = 0; j < input::EControlIndex::CONTROLINDEX; j++)
	{
		axis.controlIndex = (input::EControlIndex)j;
		for (int i = 0; i < input::EJoystickAxis::EJOYSTICKAXISSIZE; i++)
		{
			axis.axis = (input::EJoystickAxis)i;
			axis.state = 0.0f;
			input::XboxController.SetAxisValue(axis.controlIndex, axis.axis, axis.state);
		}
	}
}
EventListener::~EventListener()
{

}

/*
----------------------------------------------
public functions
----------------------------------------------
*/
EventListener::ptr EventListener::Create()
{
	return EventListener::ptr(new EventListener());
}
void EventListener::Update()
{
	input::XboxController.Update();
}

/*
----------------------------------------------
private functions
----------------------------------------------
*/

void EventListener::HandleEvent(sf::Event & pEvent)
{
	switch (pEvent.type)
	{
	case sf::Event::JoystickMoved:
		break;
	case sf::Event::JoystickButtonPressed:
		PushButtonEvent((input::EControlIndex)pEvent.joystickButton.joystickId,
			(input::EButton)pEvent.joystickButton.button, true);
		break;
	case sf::Event::JoystickButtonReleased:
		PushButtonEvent((input::EControlIndex)pEvent.joystickButton.joystickId,
			(input::EButton)pEvent.joystickButton.button, false);
		break;

	case sf::Event::JoystickConnected:
		input::XboxController.SetConnectedStatus((input::EControlIndex)pEvent.joystickConnect.joystickId, true);
		access::numberOfConnectedGamepads++;
		access::Gamepads[pEvent.joystickConnect.joystickId] = true;
		break;
	case sf::Event::JoystickDisconnected:
		input::XboxController.SetConnectedStatus((input::EControlIndex)pEvent.joystickConnect.joystickId, false);
		access::numberOfConnectedGamepads--;
		access::Gamepads[pEvent.joystickConnect.joystickId] = false;
		break;
	default:
		break;
	}
}

void EventListener::PushAxisEvent(input::EControlIndex index, input::EJoystickAxis axis, float value)
{
	input::XboxController.SetAxisValue(index, axis, value);
}
void EventListener::PushButtonEvent(input::EControlIndex index, input::EButton button, bool state)
{
	input::XboxController.SetCurrentButtonPress(index, button, state);
}

/*
----------------------------------------------
private functions
----------------------------------------------
*/






/*
----------------------------------------------
public functions
----------------------------------------------
*/
input::XBOX360Controller::XBOX360Controller()
{

}
input::XBOX360Controller::~XBOX360Controller()
{

}

bool input::XBOX360Controller::GetButtonPress(EControlIndex control, EButton button)
{
	int vectorIndex = 10 * control + input::EButton::EBUTTONSIZE * button;
	return input::XboxController.m_buttons[vectorIndex].state;
}
bool input::XBOX360Controller::GetButtonPressOnce(EControlIndex control, EButton button)
{
	int vectorIndex = 10 * control + input::EButton::EBUTTONSIZE * button;
	return (input::XboxController.m_buttons[vectorIndex].state && !input::XboxController.m_prevButtons[vectorIndex].state);
}
float input::XBOX360Controller::GetAxisValue(EControlIndex control, EJoystickAxis axis)
{
	if (fabs(sf::Joystick::getAxisPosition(control, (sf::Joystick::Axis)axis)) < 15)
		return 0.0f;

	return sf::Joystick::getAxisPosition(control, (sf::Joystick::Axis)axis);
}
bool input::XBOX360Controller::GetConnectedStatus(EControlIndex control)
{
	return m_connected[control];
}

void input::XBOX360Controller::SetCurrentButtonPress(EControlIndex control, EButton button, bool state)
{
	int vectorIndex = 10 * control + input::EButton::EBUTTONSIZE * button;
	input::XboxController.m_buttons[vectorIndex].state = state;
}
void input::XBOX360Controller::SetPreviousButtonPress(EControlIndex control, EButton button, bool state)
{
	int vectorIndex = 10 * control + input::EButton::EBUTTONSIZE * button;
	input::XboxController.m_buttons[vectorIndex].state = state;
}
void input::XBOX360Controller::SetAxisValue(EControlIndex control, EJoystickAxis axis, float state)
{
	int vectorIndex = 10 * control + input::EJoystickAxis::EJOYSTICKAXISSIZE * axis;
	input::XboxController.m_axes[vectorIndex].state = state;
}
void input::XBOX360Controller::Update()
{
	for (unsigned int i = 0; i < 40; i++)
	{
		input::XboxController.m_prevButtons[i] = input::XboxController.m_buttons[i];
	}
}
void input::XBOX360Controller::SetConnectedStatus(EControlIndex control, bool state)
{
	m_connected[control] = state;
}