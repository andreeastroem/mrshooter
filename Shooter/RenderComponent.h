/************************************************************************/
/* Base class for all rendering components
/* Includes functionality of fetching the data required to draw
/* and what type of render component it is
/************************************************************************/

#pragma once

#include "Component.h"
#include "Collider.h"

namespace mercury
{
	namespace world
	{
		enum RenderType
		{
			POINT,
			LINE,
			TRIANGLE,
			RECTANGLE,
			CIRCLE,
			POLYGON,
			SPRITE,
			TEXT,
			RENDERTYPESIZE
		};
		class RenderComponent : public Component
		{
		public:
			RenderComponent();
			~RenderComponent();

			//Inherited functions from Component
			void Initialise();
			void Update();
			void Cleanup();

			//Virtual classes
			virtual RenderType GetRenderType();
			virtual sf::Drawable* GetRenderData() = 0;
			virtual void SetCollider(Collider* pCollider = nullptr);
			virtual void SetColour(sf::Color pColour) = 0;
			virtual sf::Color GetColour() = 0{};

		private:

		public:
			
		private:

		protected:
			RenderType m_type;
			Collider* m_collider;
		};
	}
}