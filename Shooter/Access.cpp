//Access.cpp

#include "stdafx.h"

#include "Access.h"

#define PI 3.14159265359f

using namespace mercury::system;

float access::DELTATIME = 0.0f;

unsigned int access::numberOfPlayers = 0;
unsigned int access::numberOfConnectedGamepads = 0;
bool access::Gamepads[4];

namespace mercury
{
	namespace system
	{
		EComponentType access::ConvertStringToComponentEnum(std::string pString)
		{
			if (pString == "render")
				return RENDER;
			else if (pString == "input")
				return INPUT;
			else if (pString == "action")
				return ACTION;
			else if (pString == "collider")
				return COLLIDER;
			else if (pString == "bullet")
				return BULLETLOGIC;
			else if (pString == "camera")
				return CAMERA;
			else if (pString == "texteffect")
				return TEXTEFFECT;

			return ECOMPONENTTYPESIZE;

			
		}

		float access::RADTODEG(float pRadians)
		{
			return (-pRadians * (180.0f / PI));
		}

		float access::DEGTORAD(float pDegrees)
		{
			return ((-pDegrees * PI) / 180.0f);
		}

		void access::UpdateTimer(float& pTimer)
		{
			pTimer += DELTATIME;
		}

		float Math::Distance(sf::Vector2f x, sf::Vector2f y)
		{
			float dtX = x.x - y.x;
			float dtY = x.y - y.y;

			return sqrt(powf(dtX, 2) + powf(dtY, 2));
		}

		float Math::Distance(float x, float y)
		{
			return sqrt(powf(x, 2) + powf(y, 2));
		}

		float Math::Distance(sf::Vector2f point)
		{
			return sqrt(powf(point.x, 2) + powf(point.y, 2));
		}

	}
}


