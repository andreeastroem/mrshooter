//CircleComponent.cpp

#include "stdafx.h"

#include "CircleComponent.h"

namespace mercury
{
	namespace world
	{
		/************************************************************************/
		/* Constructors and deconstructors
		/************************************************************************/
		CircleComponent::CircleComponent()
		{
			m_circleShape = new sf::CircleShape;
		}

		CircleComponent::~CircleComponent()
		{

		}

		bool CircleComponent::Initialise(sf::Vector2f pPosition, float pRadius, sf::Color pColour)
		{
			m_circleShape->setPosition(pPosition);
			m_circleShape->setFillColor(pColour);
			m_circleShape->setRadius(pRadius);
			m_circleShape->setOrigin(pRadius, pRadius);

			return true;
		}

		/************************************************************************/
		/* Inherited functions
		/************************************************************************/
		sf::Drawable* CircleComponent::GetRenderData()
		{
			return m_circleShape;
		}

		void CircleComponent::Cleanup()
		{
			if (m_circleShape != nullptr)
			{
				delete m_circleShape;
				m_circleShape = nullptr;
			}
		}

		void CircleComponent::Update()
		{
			//do stuff
			m_circleShape->setPosition(m_owner->position);
		}

		void CircleComponent::SetColour(sf::Color pColour)
		{
			m_circleShape->setFillColor(pColour);
		}

		sf::Color CircleComponent::GetColour()
		{
			return m_circleShape->getFillColor();
		}

	}
}


