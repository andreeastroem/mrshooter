//Camera2D.h

#pragma once

#include "Component.h"

namespace mercury
{
	namespace world
	{
		class Camera2D : public Component
		{
		public:
			Camera2D();
			~Camera2D();

			//Essential functions
			void Initialise(sf::View pView, unsigned int pId);
			void Initialise(sf::FloatRect pRect, unsigned int pId);
			void Update();
			void Cleanup();
			void OnCollision();

			void ChangeViewSize(sf::FloatRect pRect);
			void ChangeScreenArea(sf::Vector2f pArea);
			sf::View GetView();
		private:

		public:
			float id;
		private:
			sf::View m_view;
		};
	}
}