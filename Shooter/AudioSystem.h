//AudioSystem.h

#pragma once

#include <memory>

namespace mercury
{
	namespace system
	{
		class AudioSystem
		{
		public:

			~AudioSystem();

			static sf::SoundBuffer* LoadSound(const std::string& pFilename);
			static void PlayMusic(const std::string& pFilename);
			static void StopMusic();

		private:
			AudioSystem();
		public:

		private:
			static std::map<std::string, sf::SoundBuffer> m_sounds;
			static std::string m_directory;
			static sf::Music m_music;
		};
	}
}