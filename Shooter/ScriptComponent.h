/************************************************************************/
/* Base component for scripts
/* Scripts will be the the goto way of creating gameplay and disconnect
/* different objects from each other
/************************************************************************/

#pragma once

#include "Component.h"

namespace mercury
{
	namespace world
	{
		class Collider;

		class ScriptComponent : public Component
		{
		public:

			ScriptComponent();
			~ScriptComponent();


			void Cleanup();
			void Update();
			void OnCollision(Collider* pCollider);


		private:

		protected:

		};
	}
}