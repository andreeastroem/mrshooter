//Component.h

#pragma once

#include "GameObject.h"
#include <string>

namespace mercury
{
	namespace world
	{
		class Collider;

		class Component
		{
		public:
			//Constructor
			Component();

			//Deconstructor
			~Component();

			//Essential functions
			virtual void SetOwner(GameObject* pOwner);

			virtual void Cleanup() = 0;
			virtual void Update() = 0;
			virtual void OnCollision(Collider* pCollider);
			virtual void ExitCollision(Collider* pCollider);
			virtual bool getFlag();
			virtual void setFlag(bool state);

			//Access functions


			//Public variables
			unsigned int ID;
			std::string type;
		protected:
			GameObject* m_owner;

			bool m_flag;
		};
	}
}