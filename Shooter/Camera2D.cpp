//Camera2D.cpp

#include "stdafx.h"

#include "Camera2D.h"

namespace mercury
{
	namespace world
	{


		Camera2D::Camera2D()
		{
			type = "camera";
		}

		Camera2D::~Camera2D()
		{

		}

		void Camera2D::Initialise(sf::View pView, unsigned int pId)
		{
			m_view = pView;
			id = (float)pId;
		}

		void Camera2D::Initialise(sf::FloatRect pRect, unsigned int pId)
		{
			m_view.setViewport(pRect);
			id = (float)pId;
		}

		void Camera2D::Update()
		{
			m_view.setCenter(m_owner->position);
		}

		void Camera2D::Cleanup()
		{
			Component::Cleanup();
		}

		void Camera2D::OnCollision()
		{
			
		}

		void Camera2D::ChangeViewSize(sf::FloatRect pRect)
		{
			m_view.setViewport(pRect);
		}

		sf::View Camera2D::GetView()
		{
			return m_view;
		}

		void Camera2D::ChangeScreenArea(sf::Vector2f pArea)
		{
			const float left = m_view.getViewport().left;
			const float top = m_view.getViewport().top;
			m_view.setViewport(sf::FloatRect(left, top, pArea.x, pArea.y));
		}

	}
}