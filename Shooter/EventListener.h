//EventListener.h

#pragma once

#include <unordered_map>
#include <memory>

namespace mercury
{
	class Engine;
	namespace system
	{
		namespace input
		{
			enum EButton
			{
				A,
				B,
				Y,
				X,
				LEFTSHOULDER,
				RIGHTSHOULDER,
				BACK,
				START,
				LEFTSTICK,
				RIGHTSTICK,
				EBUTTONSIZE
			};
			enum EJoystickAxis
			{
				XAXIS,
				YAXIS,
				ZAXIS,
				RAXIS,
				UAXIS,
				VAXIS,
				POVYAXIS,
				POVXAXIS,
				EJOYSTICKAXISSIZE
			};
			enum EControlIndex
			{
				ONE,
				TWO,
				THREE,
				FOUR,
				CONTROLINDEX
			};
			struct GamepadButton
			{
				EControlIndex controlIndex;
				EButton button;
				bool state;
			};
			struct GamepadAxis
			{
				EControlIndex controlIndex;
				EJoystickAxis axis;
				float state;
			};
			
			class XBOX360Controller
			{
			public:
				bool GetButtonPress(EControlIndex control, EButton button);
				bool GetButtonPressOnce(EControlIndex control, EButton button);
				float GetAxisValue(EControlIndex control, EJoystickAxis axis);
				bool GetConnectedStatus(EControlIndex control);

				void SetCurrentButtonPress(EControlIndex control, EButton button, bool state);
				void SetPreviousButtonPress(EControlIndex control, EButton button, bool state);
				void SetAxisValue(EControlIndex control, EJoystickAxis axis, float value);
				void SetConnectedStatus(EControlIndex control, bool state);

				void Update();

				XBOX360Controller();
				~XBOX360Controller();
			private:

				GamepadButton m_buttons[40];
				GamepadButton m_prevButtons[40];
				GamepadAxis m_axes[28];

				bool m_connected[4];

				/*static std::vector<GamepadButton> m_buttons;
				static std::vector<GamepadButton> m_prevButtons;
				static std::vector<GamepadAxis> m_axes;*/
			};

			static XBOX360Controller XboxController;

			/*
			class XBOX360Controller
			{
			public:
				static void Create();

				bool GetButtonPress(EControlIndex control, EButton button);
				bool GetButtonPressOnce(EControlIndex control, EButton button);
				float GetAxisValue(EControlIndex control, EJoystickAxis axis);

			private:
				XBOX360Controller();

				std::vector<GamepadButton> m_buttons;
				std::vector<GamepadButton> m_prevButtons;
				std::vector<GamepadAxis> m_axes;
			};*/
		}

		class EventListener
		{
			friend class Engine;
		public:
			typedef std::unique_ptr<EventListener> ptr;
			static ptr Create();

			void Update();

			//Managed automaticly
			~EventListener();
		private:
			EventListener();

			void HandleEvent(sf::Event& pEvent);

			void PushAxisEvent(input::EControlIndex index, input::EJoystickAxis axis, float value);
			void PushButtonEvent(input::EControlIndex index, input::EButton button, bool state);
		public:

		private:
			
		};
	}
}