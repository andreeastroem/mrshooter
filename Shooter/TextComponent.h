//TextComponent.h

#pragma once

#include "RenderComponent.h"

namespace mercury
{
	namespace world
	{
		class TextComponent : public RenderComponent
		{
		public:
			TextComponent();
			~TextComponent();

			void Cleanup();
			void Update();

			bool Initialise(sf::Vector2f pPosition, std::string pText);
		
			void SetTextPosition(const sf::Vector2f& pPosition);
			void SetTextScale(const sf::Vector2f& pScale);
			void SetString(const char* pText);

			sf::Text* getText();
			sf::Drawable* getTextDrawable();

			sf::Drawable* GetRenderData();

			void SetColour(sf::Color pColour);

			sf::Color GetColour();

		private:

		public:

		private:
			sf::Text* m_text;
			sf::Font* m_font;
		};
	}
}