//GameObject.h

#pragma once

#include <string>


namespace mercury
{
	namespace world
	{
		class Component;
		class Collider;

		enum EFaction
		{
			RED,
			BLUE,
			GREEN,
			YELLOW,
			NEUTRAL,
			HOSTILE,
			EFACTIONSIZE
		};
		
		class GameObject
		{
		public:

			GameObject();
			GameObject(std::string pName); 
			GameObject(std::string pName, sf::Vector2f pPosition);
			~GameObject();

			//Essential functions
			void Initialise(EFaction pFaction);
			void Update();
			void AttachComponent(system::EComponentType pType, Component* pComponent);
			void Cleanup();
			void OnCollision(Collider* pCollider);
			void ExitCollision(Collider* pCollider);

			//void AddForce(sf::Vector2f pForce);

			//Access functions
			template <class T>
			bool HasComponentOfType(system::EComponentType pType);
			template <class T>
			T* GetComponent(system::EComponentType pType);

			bool GetDeathFlag() const;
			void SetDeathFlag(bool pState);
			EFaction GetFaction() const;
		private:

		public:
			std::string name;		//identifier
			static unsigned int ID;
			sf::Vector2f position;
			float rotation;
			sf::Vector2f direction;
			

		private:
			bool m_deathFlag;
			EFaction m_faction;

			Component* m_components[system::EComponentType::ECOMPONENTTYPESIZE];

		};

		/************************************************************************/
		/* Access template classes
		/************************************************************************/

		template <class T>
		bool GameObject::HasComponentOfType(system::EComponentType pType)
		{
			if (m_components[pType] != nullptr)	//validate component of type
				return true;

			return false;						//Else
		}

		template <class T>
		T* GameObject::GetComponent(system::EComponentType pType)
		{
			if (HasComponentOfType<T>(pType))
				return static_cast<T*>(m_components[pType]);
			else
				return nullptr;
		}
	}
}

