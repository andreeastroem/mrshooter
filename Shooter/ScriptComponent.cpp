
#include "stdafx.h"

#include "ScriptComponent.h"
#include "Collider.h"

namespace mercury
{
	namespace world
	{

		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		ScriptComponent::ScriptComponent()
		{

		}

		ScriptComponent::~ScriptComponent()
		{

		}

		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		void ScriptComponent::Cleanup()
		{

		}

		void ScriptComponent::Update()
		{

		}

		void ScriptComponent::OnCollision(Collider* pCollider)
		{
			pCollider;
		}

	}
}