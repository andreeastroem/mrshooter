//BulletLogic.h

#pragma once

#include "Component.h"

namespace mercury
{
	namespace world
	{
		class BulletLogic : public Component
		{
		public:
			BulletLogic();
			~BulletLogic();

			//Essential functions
			void Initialise(sf::Vector2f pDirection, float pSpeed, float pLifespan);
			void Update();
			void Cleanup();
			void OnCollision(Collider* pCollider);

		private:
			sf::Vector2f m_direction;
			float m_speed;
			float m_lifespan;
		};
	}
}