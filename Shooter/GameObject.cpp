//GameObject.cpp

#include "stdafx.h"

#include "Component.h"
#include "Collider.h"

#include "GameObject.h"

namespace mercury
{
	namespace world
	{
		unsigned int GameObject::ID = 0;

		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		GameObject::GameObject()
		{
			name = "Object";
			ID++;
			m_deathFlag = false;
			
			position = sf::Vector2f(0.0f, 0.0f);
			rotation = 0.0f;
			direction = sf::Vector2f(1.0f, 0.0f);

			for (unsigned int i = 0; i < system::EComponentType::ECOMPONENTTYPESIZE; i++)
			{
				m_components[i] = nullptr;
			}
		}
		GameObject::GameObject(std::string pName)
		{
			ID++;
			m_deathFlag = false;

			position = sf::Vector2f(0.0f, 0.0f);
			rotation = 0.0f;
			direction = sf::Vector2f(1.0f, 0.0f);

			for (unsigned int i = 0; i < system::EComponentType::ECOMPONENTTYPESIZE; i++)
			{
				m_components[i] = nullptr;
			}

			name = pName;
		}
		GameObject::GameObject(std::string pName, sf::Vector2f pPosition)
		{
			ID++;
			m_deathFlag = false;

			rotation = 0.0f;
			direction = sf::Vector2f(1.0f, 0.0f);

			for (unsigned int i = 0; i < system::EComponentType::ECOMPONENTTYPESIZE; i++)
			{
				m_components[i] = nullptr;
			}

			name = pName;
			position = pPosition;
		}

		GameObject::~GameObject()
		{
		}


		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		void GameObject::Initialise(EFaction pFaction)
		{
			m_faction = pFaction;
		}
		void GameObject::Update()
		{
			for (unsigned int i = 0; i < system::EComponentType::ECOMPONENTTYPESIZE; i++)
			{
				if (m_components[i] != nullptr)
					m_components[i]->Update();
			}

		}

		void GameObject::Cleanup()
		{
			for (unsigned int i = 0; i < system::EComponentType::ECOMPONENTTYPESIZE; i++)
			{
				if (m_components[i])
				{
					m_components[i]->Cleanup();
					delete m_components[i];
					m_components[i] = nullptr;
				}
			}
		}
		void GameObject::OnCollision(Collider* pCollider)
		{
			for (unsigned int i = 0; i < system::EComponentType::ECOMPONENTTYPESIZE; i++)
			{
				if (m_components[i])
					m_components[i]->OnCollision(pCollider);
			}
		}
		void GameObject::ExitCollision(Collider* pCollider)
		{
			for (unsigned int i = 0; i < system::EComponentType::ECOMPONENTTYPESIZE; i++)
			{
				if (m_components[i])
					m_components[i]->ExitCollision(pCollider);
			}
		}
		bool GameObject::GetDeathFlag() const
		{
			return m_deathFlag;
		}
		void GameObject::SetDeathFlag(bool pState)
		{
			m_deathFlag = pState;
		}

		EFaction GameObject::GetFaction() const
		{
			return m_faction;
		}

		void GameObject::AttachComponent(system::EComponentType pType, Component* pComponent)
		{
			m_components[pType] = pComponent;
		}


		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/

	}
}