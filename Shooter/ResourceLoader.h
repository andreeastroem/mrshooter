/************************************************************************/
/* ResourceLoader.h
/* This class loads resources on multiple threads for a 
/* slight performance boost
/************************************************************************/

#pragma once

#include <memory>

#include <thread>
#include <mutex>

namespace mercury
{
	namespace system
	{
		enum LOAD_RESULT
		{
			FAILURE,
			SUCCESS
		};
		class ResourceLoader
		{
		public:
			/************************************************************************/
			/* Initialisation
			/************************************************************************/
			typedef std::unique_ptr<ResourceLoader> ptr;
			static ptr Create();
		private:
			ResourceLoader(){};
		public:
			void CreateTexture(std::string pPathName);
			void CreateSound(std::string pPathName);
		public:
			/************************************************************************/
			/* Resource loading
			/************************************************************************/
			int CreateTextureFromFile(const std::string& pPathName);
			int CreateSoundFromFile(const std::string& pPathNameint);
			LOAD_RESULT LoadSection(const std::string& pFileName, const std::string& pSectionName);
			LOAD_RESULT LoadInt(const std::string& pSection, const std::string& pVariableName, int& pValue);
			LOAD_RESULT LoadBool(const std::string& pSection, const std::string& pVariableName, bool& pValue);

			/************************************************************************/
			/* Access functions
			/************************************************************************/
			sf::Texture* GetTexture(unsigned int pID);
			sf::SoundBuffer* GetSound(unsigned int pID);
		private:
			bool FindSection(const std::string& pSectionName, std::string pLine);

		private:
			std::vector<sf::Texture> m_textureCache;
			std::vector<sf::SoundBuffer> m_soundCache;
			std::vector<sf::Music> m_musicCache;

			std::map<std::string, std::vector<std::string>> m_sections;

			std::mutex m_mutex;
			std::thread m_textureThread, m_soundBufferThread, m_configreaderThread;
		};
	}
}