//RenderComponent.cpp

#include "stdafx.h"

#include "RenderComponent.h"
#include "EventListener.h"


namespace mercury
{
	namespace world
	{
		/************************************************************************/
		/* Constructors and deconstructor
		/************************************************************************/
		RenderComponent::RenderComponent()
		{

		}

		RenderComponent::~RenderComponent()
		{

		}

		/************************************************************************/
		/* Inherited functions
		/************************************************************************/
		void RenderComponent::Initialise()
		{

		}

		void RenderComponent::Update()
		{

		}

		void RenderComponent::Cleanup()
		{

		}

		/************************************************************************/
		/* Virtual classes
		/************************************************************************/
		RenderType RenderComponent::GetRenderType()
		{
			return m_type;
		}

		void RenderComponent::SetCollider(Collider* pCollider)
		{
			m_collider = pCollider;
		}

		void RenderComponent::SetColour(sf::Color pColour)
		{
			(void)pColour;
		}

	}
}